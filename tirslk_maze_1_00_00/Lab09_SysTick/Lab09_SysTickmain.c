// Lab09_SysTickmain.c
// Runs on MSP432
// Student version of SysTick, lab9
// Additional comments by Capt Steven Beyer
// 7 Aug 2019
// Daniel and Jonathan Valvano
// February 19, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// built-in LED1 connected to P1.0
// negative logic built-in Button 1 connected to P1.1
// negative logic built-in Button 2 connected to P1.4
// built-in red LED connected to P2.0
// built-in green LED connected to P2.1
// built-in blue LED connected to P2.2
// RC circuit connected to P2.6, to create DAC
#include <stdint.h>
#include "msp.h"
#include "..\inc\TExaS.h"
#include "..\inc\Clock.h"
#include "..\inc\CortexM.h"
#include "..\inc\SysTick.h"
#include "..\inc\LaunchPad.h"
uint32_t const DutyBuf[100]={
    240000, 255063, 270067, 284953, 299661, 314133, 328313, 342144, 355573, 368545,
    381010, 392918, 404223, 414880, 424846, 434083, 442554, 450226, 457068, 463053,
    468158, 472363, 475651, 478008, 479427, 479900, 479427, 478008, 475651, 472363,
    468158, 463053, 457068, 450226, 442554, 434083, 424846, 414880, 404223, 392918,
    381010, 368545, 355573, 342144, 328313, 314133, 299661, 284953, 270067, 255063,
    240000, 224937, 209933, 195047, 180339, 165867, 151687, 137856, 124427, 111455,
    98990, 87082, 75777, 65120, 55154, 45917, 37446, 29774, 22932, 16947,
    11842, 7637, 4349, 1992, 573, 100, 573, 1992, 4349, 7637,
    11842, 16947, 22932, 29774, 37446, 45917, 55154, 65120, 75777, 87082,
    98990, 111455, 124427, 137856, 151687, 165867, 180339, 195047, 209933, 224937
};
const uint32_t PulseBuf[100]={
    5000, 5308, 5614, 5918, 6219, 6514, 6804, 7086, 7361, 7626,
    7880, 8123, 8354, 8572, 8776, 8964, 9137, 9294, 9434, 9556,
    9660, 9746, 9813, 9861, 9890, 9900, 9890, 9861, 9813, 9746,
    9660, 9556, 9434, 9294, 9137, 8964, 8776, 8572, 8354, 8123,
    7880, 7626, 7361, 7086, 6804, 6514, 6219, 5918, 5614, 5308,
    5000, 4692, 4386, 4082, 3781, 3486, 3196, 2914, 2639, 2374,
    2120, 1877, 1646, 1428, 1224, 1036,  863,  706,  566,  444,
     340,  254,  187,  139,  110,  100,  110,  139,  187,  254,
     340,  444,  566,  706,  863, 1036, 1224, 1428, 1646, 1877,
    2120, 2374, 2639, 2914, 3196, 3486, 3781, 4082, 4386, 4692};
	
// 9.4.1 - Write code to utilize SysTick to wait for 1 us
// Three registers used: LOAD, VAL, and CTRL
void SysTick_Wait1us(uint32_t delay){
    SysTick->LOAD = 48*delay - 1; //writing delay value
    SysTick->VAL = 0; //clear VAL
    while((SysTick->CTRL&0x00010000) == 0) {}; //wait for COUNT bit in CTRL reg to be set
}

// 9.4.1 - Program to test wait function. Signal created
// should be high for 7.5 ms and low for 2.5 ms
int Program9_1(void){
  Clock_Init48MHz();  // makes bus clock 48 MHz
  SysTick_Init();
  LaunchPad_Init();   // buttons and LEDs
  TExaS_Init(LOGICANALYZER_P1);
  while(1){
    P1->OUT |= 0x01;   // red LED on
    SysTick_Wait1us(7500);
    P1->OUT &= ~0x01;  // red LED off
    SysTick_Wait1us(2500);
  }
}

// 9.4.2
// Operation
// When beating, the P1.0 LED oscillates at 100 Hz (too fast to see with the eye)
// and the duty cycle is varied sinusoidally once a second
int Program9_2(void){ 
  Clock_Init48MHz(); // makes it 48 MHz
  TExaS_Init(LOGICANALYZER_P1);
  LaunchPad_Init();   // buttons and LEDs
  SysTick_Init();
  uint32_t H, L;
  int i = 0;
  EnableInterrupts();
  while(1){
      H = PulseBuf[i]; //look up new H value
      L = 10000 - H; //calc L value
      P1->OUT |= 0x01; //set P1.0 high
      SysTick_Wait1us(H); //wait H us
      P1->OUT &= ~0x01; //set P1.0 low
      SysTick_Wait1us(L); //wait L us
      i++;
      if(i == 100) {
          i = 0;
      }
  }
}

// 9.4.3
// Operation
// The heartbeat starts when the operator pushes Button 1
// The heartbeat stops when the operator pushes Button 2
// When beating, the P1.0 LED oscillates at 100 Hz (too fast to see with the eye)
// and the duty cycle is varied sinusoidally once a second
int Program9_3(void){
	Clock_Init48MHz(); // makes it 48 MHz
	TExaS_Init(LOGICANALYZER_P1);
	LaunchPad_Init();   // buttons and LEDs
	SysTick_Init();
	uint32_t H, L;
	int i = 0;
	uint8_t one, four;
	EnableInterrupts();
	while(1) {
	    one = P1->IN&0x02; //initial P1.1 reading
	    four = P1->IN&0x10; //initial P1.4 reading
	    if(one == 0) {
	        while(four != 0) {
	            H = PulseBuf[i]; //look up new H value
	            L = 10000 - H; //calc L value
	            P1->OUT |= 0x01; //set P1.0 high
	            SysTick_Wait1us(H); //wait H us
	            P1->OUT &= ~0x01; //set P1.0 low
	            SysTick_Wait1us(L); //wait L us
	            i++;
	            if(i == 100) {
	                i = 0;
	            }
	            four = P1->IN&0x10; //check if P1.4 is pressed
	        }
	    }
	    if(four == 0) {
	        while(one != 0) {
	            P1->OUT &= ~0x01; //set P1.0 low
	            one = P1->IN&0x02; //check if P1.1 is pressed, either 1 or 0
	        }
	    }
	}
}


// 9.4.4 - Program to test PWM DAC - listed as Program9_2 in the LAB
int Program9_4(void){uint32_t H,L;
  Clock_Init48MHz();  // makes bus clock 48 MHz
  SysTick_Init();
  TExaS_Init(SCOPE);
  P2->SEL0 &= ~0x40;
  P2->SEL1 &= ~0x40; // 1) configure P2.6 as GPIO
  P2->DIR |= 0x40;   // P2.6 output
  H = 7500;
  L = 10000-H;
  while(1){
    P2->OUT |= 0x40;   // on
    SysTick_Wait1us(H);
    P2->OUT &= ~0x40;  // off
    SysTick_Wait1us(L);
  }
}



void main(void){
  // run one of these
  //Program9_1();
  //Program9_2();
  Program9_3();
//  Program9_4();
}



