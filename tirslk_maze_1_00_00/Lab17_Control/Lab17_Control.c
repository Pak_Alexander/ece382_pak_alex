// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// September 12, 2017

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/FlashProgram.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"
#include "../inc/Classifier.h"


/**************Initial values used for all programs******************/
#define PWMNOMINAL 5000
#define SWING 900 //change back to 3000 when not using IR sensors, use 900 when using IR sensors
#define PWMIN (PWMNOMINAL-SWING)
#define PWMAX (PWMNOMINAL+SWING)

volatile uint32_t ControllerFlag; // set every 10ms on controller execution

int32_t Second_Bump = 0; //added semaphore for second bump for level 1
int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec

// proportional controller gain 
// experimentally determine value that creates a stable system
// may want a different gain for each program
int32_t Kp= 8;

/**************Functions used by all programs***********************/
void Pause3(void){
    int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
    Clock_Delay1ms(200); LaunchPad_Output(4); // blue
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // restart Jacki
  UR = UL = PWMNOMINAL;    // reset parameters
  Mode = 1;
  Second_Bump = 0; //added
  ControllerFlag = 0;
  Time = 0;
}

/**************Program17_1******************************************/
#define DESIRED_SPEED 200
#define TACHBUFF 10                      // number of elements in tachometer array

int32_t ActualSpeedL, ActualSpeedR;   	 // Actual speed
int32_t ErrorL, ErrorR;     			 // X* - X'
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)

int i = 0;

void LCDClear1(void){
    Nokia5110_Init();
    Nokia5110_Clear();
    Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Error(RPM)  L     R     ");
}
void LCDOut1(void){
    Nokia5110_SetCursor(1, 1);         // one leading space, second row
    Nokia5110_OutSDec1(DESIRED_SPEED);
    Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
    Nokia5110_OutSDec1(DESIRED_SPEED);
    Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
    Nokia5110_OutUDec(ActualSpeedL);
    Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
    Nokia5110_OutUDec(ActualSpeedR);
    Nokia5110_SetCursor(1, 5);       // zero leading spaces, sixth row
    Nokia5110_OutSDec1(ErrorL);
    Nokia5110_SetCursor(7, 5);       // six leading spaces, sixth row
    Nokia5110_OutSDec1(ErrorR);
}

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;
  for(i=0; i<length; i=i+1){
    sum = sum + array[i];
  }
  return (sum/length);
}

//**************************************************
// Proportional controller to drive straight with no
// sensor input
void Controller1(void){

    if(Mode){
		// pull tachometer information
		Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
		i++;
		
		if(i >= TACHBUFF) {
		    i = 0;
		    // use average of ten tachometer values to determine
		    // actual speed (similar to Lab16)
		    uint16_t averageL = avg(LeftTach,TACHBUFF);
		    uint16_t averageR = avg(RightTach,TACHBUFF);
		    ActualSpeedL = 2000000 / averageL;
		    ActualSpeedR = 2000000 / averageR;

		    //calculated error to account for in the proportional control
		    ErrorL = DESIRED_SPEED - ActualSpeedL;
		    ErrorR = DESIRED_SPEED - ActualSpeedR;
		    // use proportional control to update duty cycle
		    // LeftDuty = LeftDuty + Kp*LeftError
		    UR = UR + Kp * ErrorR;
		    UL = UL + Kp * ErrorL;

		    // check min/max duty values
		    if(UR < 2) {
		        UR = 2;
		    }
		    if(UR > 14998) {
		        UR = 14998;
		    }
		    if(UL < 2) {
		        UL = 2;
		    }
		    if(UL > 14998) {
		        UL = 14998;
		    }
		    // update motor values
		    Motor_Forward(UL, UR);
		    ControllerFlag = 1;
		}
    }
}

// go straight with no sensor input
void Program17_1(void){
  DisableInterrupts();
// initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Tachometer_Init();
  Motor_Init();
  
  // user TimerA1 to run the controller at 100 Hz
  TimerA1_Init(&Controller1, 5000);
  
  Motor_Stop();
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  LCDClear1();
  ControllerFlag = 0;
  Pause3();

	while(1){
		if(Bump_Read()){
			Mode = 0;
			Motor_Stop();
			Pause3();
		}
		if(ControllerFlag){
			LCDOut1();
			ControllerFlag = 0;
		}
  }
}

/**************Program17_2******************************************/
// distances in mm
#define TOOCLOSE 110
#define DESIRED_DIST 172
#define TOOFAR 230

volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm
volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosError;
int32_t Error2;

void LCDClear2(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("IR distance");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("L= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("C= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("R= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("E= "); Nokia5110_OutSDec(0); Nokia5110_OutString(" mm");
}

void LCDOut2(void){
  Nokia5110_SetCursor(3,2); Nokia5110_OutSDec(Left);
  Nokia5110_SetCursor(3,3); Nokia5110_OutSDec(Center);
  Nokia5110_SetCursor(3,4); Nokia5110_OutSDec(Right);
  Nokia5110_SetCursor(3,5); Nokia5110_OutSDec(Error2);
  // left
  if(Time%5 == 0){
      UART0_OutUDec5(Left);UART0_OutString(" mm,");
      UART0_OutUDec5(Center);UART0_OutString(" mm,");
      UART0_OutUDec5(Right);UART0_OutString(" mm,");
      UART0_OutUDec5(UR);UART0_OutString(" %,");
      UART0_OutUDec5(UL);UART0_OutString(" %,");
      if(Error2 < 0){
          PosError = Error2*(-1);
          UART0_OutString("-");UART0_OutUDec5(PosError);UART0_OutString("\n");
      }
      else{
          UART0_OutUDec5(Error2);UART0_OutString("\n");
      }

  }
}

void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_12_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
//void SysTick_Handler(void){
//    if(Mode){
//		// Determine set point
//        if(Left > 172 && Right > 172) {
//            SetPoint = (Left + Right) / 2;
//        } else {
//            SetPoint = 172;
//        }
//
//	    //set error based off set point
//        if(Left < Right) {
//            Error2 = Left - SetPoint;
//        }
//        if(Right < Left) {
//            Error2 = SetPoint - Right;
//        }
//
//		// update duty cycle based on proportional control
//		UR = UR + Kp * Error2;
//		UL = UL - Kp * Error2;
//
//		// check to ensure not too big of a swing
//		if(UR < PWMIN) {
//		    UR = PWMIN;
//		}
//		if(UR > PWMAX) {
//		    UR = PWMAX;
//		}
//		if(UL < PWMIN) {
//		    UL = PWMIN;
//		}
//		if(UL > PWMAX) {
//		    UL = PWMAX;
//		}
//
//		// update motor values
//        Motor_Forward(UL, UR);
//        ControllerFlag = 1;
//    }
//}

// proportional control, wall distance
void Program17_2(void){
    uint32_t raw17,raw12,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init(); //change this from lab 14 Bumpint_Init()
    Motor_Init();
	
	// user TimerA1 to sample the IR sensors at 2000 Hz
	TimerA1_Init(&IRsampling, 250);
	
    Motor_Stop();
    LCDClear2();
    Mode = 0;
    UR = UL = PWMNOMINAL;
    ADCflag = ControllerFlag = 0;   // semaphores

    ADC0_InitSWTriggerCh17_12_16();   // initialize channels 17,12,16
    ADC_In17_12_16(&raw17,&raw12,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw12,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

	// user SysTick to run the controller at 100 Hz with a priority of 2
	SysTick_Init(480000, 2);
	
    Pause3();

    EnableInterrupts();
    while(1){
        if(Bump_Read()){ // collision
            Mode = 0;
            Motor_Stop();
            Pause3();
        }
        if(ControllerFlag){ // 100 Hz, not real time
            LCDOut2();
            ControllerFlag = 0;
        }
    }
}

/**************Program17_3******************************************/
uint8_t LineData;       // direct measure from line sensor
int32_t Position;      // position in 0.1mm relative to center of line

void LCDClear3(void){
  Nokia5110_Init();
  Nokia5110_Clear(); // erase entire display
  Nokia5110_OutString("17: control");
  Nokia5110_SetCursor(0,1); Nokia5110_OutString("Line Follow");
  Nokia5110_SetCursor(0,2); Nokia5110_OutString("D =  "); Nokia5110_OutUDec(0);
  Nokia5110_SetCursor(0,3); Nokia5110_OutString("P = "); Nokia5110_OutSDec(0);
  Nokia5110_SetCursor(0,4); Nokia5110_OutString("UR=  "); Nokia5110_OutUDec(0);
  Nokia5110_SetCursor(0,5); Nokia5110_OutString("UL=  "); Nokia5110_OutUDec(0);
}
void LCDOut3(void){
  Nokia5110_SetCursor(5,2); Nokia5110_OutUDec(LineData);
  Nokia5110_SetCursor(4,3); Nokia5110_OutSDec(Position);
  Nokia5110_SetCursor(5,4); Nokia5110_OutUDec(UR);
  Nokia5110_SetCursor(5,5); Nokia5110_OutUDec(UL);
}

//collision was created for level 1 in order to stop the robot when it bumps into the wall for the second time and make it turn 180 degrees when
//it bumps into the wall for the first time
void Collision(void) {
    Mode = 0;
    Motor_Stop();
    if(Second_Bump == 1) { //if the robot bumped into the wall the second time
        Pause3(); //modified to flash red and blue
    } else {
        Motor_Right(6000, 6000); //turn 180
        Clock_Delay1ms(1000);
        Second_Bump = 1; //trigger semaphore
        Mode = 1; //resume normal operation
    }
}

/*
* Proportional controller to drive robot 
* using line following
*/
int32_t change=0;
void Controller3(void){
	// read values from line sensor, similar to
	// SysTick_Handler() in Lab10_Debugmain.c
    Time++;
    if (Time%10==0){
        Reflectance_Start();
    }
    if (Time%10==1){
        LineData = Reflectance_End();
    }

    // Use Reflectance_Position() to find position
    Position = Reflectance_Position(LineData);

    if(Mode){
        // update duty cycle based on proportional control
        UR = PWMNOMINAL + Kp * Position;
        UL = PWMNOMINAL - Kp * Position;

		// limit change to within swing
        if(UR < PWMIN) {
            UR = PWMIN;
        }
        if(UR > PWMAX) {
            UR = PWMAX;
        }
        if(UL < PWMIN) {
            UL = PWMIN;
        }
        if(UL > PWMAX) {
            UL = PWMAX;
        }

        //update the motor PWM values
        Motor_Forward(UL, UR);
		ControllerFlag = 1;
    }
}

// proportional control, line following
void Program17_3(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    
	// user TimerA1 to run the controller at 1000 Hz
	TimerA1_Init(&Controller3, 500);
	
    Motor_Stop();
    LCDClear3();
    Second_Bump = 0; //added for level 1
    Mode = 0;
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();
    Pause3();
    while(1){
      if(Bump_Read()){ // collision
        //Mode = 0;
        //Motor_Stop();
        //Pause3();
        Collision(); //added for level 1
      }
      if(ControllerFlag){ // 100 Hz , not real time
        LCDOut3();
        ControllerFlag = 0;
      }
    }
}

/**************Level 2******************************************/
//added for level 2, cases to determine what the robot should do when it sees a pattern in the line when navigating through maze
void Turn(uint8_t data) {
    if(Bump_Read()){ // collision
        Mode = 0;
        Motor_Stop();
        Pause3();
    }
    if(data >= 0xF0 || data == 0xFF) { //if the robot sees a t intersection or line is 90 degrees to left, turn 90 degrees left
        Motor_Stop();
        Clock_Delay1ms(500);
        Motor_Left(2000, 5000);
        Clock_Delay1ms(1000);
    }
    if(data == 0x00) { //if the robot reaches a dead end, then turn 180
        Motor_Stop();
        Clock_Delay1ms(500);
        Motor_Left(6000, 6000);
        Clock_Delay1ms(1000);
    }
    if(data == 0xDB || data == 0x5B) { //if the robot sees the treasure, stop and flash red and blue
        while(1){
            Motor_Stop();
            Clock_Delay1ms(200); LaunchPad_Output(0); // off
            Clock_Delay1ms(200); LaunchPad_Output(1); // red
            Clock_Delay1ms(200); LaunchPad_Output(4); // blue
        }
    }
}
/*
* Proportional controller to drive robot
* using line following
*/
void Controller4(void){
    // read values from line sensor, similar to
    // SysTick_Handler() in Lab10_Debugmain.c
    Time++;
    if (Time%10==0){
        Reflectance_Start();
    }
    if (Time%10==1){
        LineData = Reflectance_End();
        if(Mode){
            Turn(LineData); //added functionality for level 2, used to determine what to do if it sees certain patterns when reading lines in maze
        }
    }

    // Use Reflectance_Position() to find position
    Position = Reflectance_Position(LineData);

    if(Mode){
        // update duty cycle based on proportional control
        UR = PWMNOMINAL + Kp * Position;
        UL = PWMNOMINAL - Kp * Position;

        // limit change to within swing
        if(UR < PWMIN) {
            UR = PWMIN;
        }
        if(UR > PWMAX) {
            UR = PWMAX;
        }
        if(UL < PWMIN) {
            UL = PWMIN;
        }
        if(UL > PWMAX) {
            UL = PWMAX;
        }
        //update the motor PWM values
        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }
}

//similar to Program17_3()
void Level_2(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();

    // user TimerA1 to run the controller at 1000 Hz
    TimerA1_Init(&Controller4, 500);

    Motor_Stop();
    LCDClear3();
    Mode = 0;
    Time = 0;
    UR = UL = PWMNOMINAL;
    EnableInterrupts();
    Pause3();
    while(1){
      if(Bump_Read()){ // collision
        Mode = 0;
        Motor_Stop();
        Pause3();
      }
      if(ControllerFlag){ // 100 Hz , not real time
        LCDOut3();
        ControllerFlag = 0;
      }
    }
}

/**************Level 3******************************************/
void Controller5(void){

    if(Mode){
        // pull tachometer information
        Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
        i++;

        if(i >= TACHBUFF) {
            i = 0;
            // use average of ten tachometer values to determine
            // actual speed (similar to Lab16)
            uint16_t averageL = avg(LeftTach,TACHBUFF);
            uint16_t averageR = avg(RightTach,TACHBUFF);
            ActualSpeedL = 2000000 / averageL;
            ActualSpeedR = 2000000 / averageR;

            //calculated error to account for in the proportional control
            ErrorL = DESIRED_SPEED - ActualSpeedL;
            ErrorR = DESIRED_SPEED - ActualSpeedR;
            // use proportional control to update duty cycle
            // LeftDuty = LeftDuty + Kp*LeftError
            UR = UR + Kp * ErrorR;
            UL = UL + Kp * ErrorL;

            // check min/max duty values
            if(UR < 2) {
                UR = 2;
            }
            if(UR > 14998) {
                UR = 14998;
            }
            if(UL < 2) {
                UL = 2;
            }
            if(UL > 14998) {
                UL = 14998;
            }
            // update motor values
            Motor_Forward(UL, UR);
            ControllerFlag = 1;
        }
    }
}

// go straight with no sensor input
void Level_3(void){
  DisableInterrupts();
// initialization
  Clock_Init48MHz();
  LaunchPad_Init();
  Bump_Init();
  Tachometer_Init();
  Motor_Init();

  // user TimerA1 to run the controller at 100 Hz
  TimerA1_Init(&Controller5, 5000);

  Motor_Stop();
  UR = UL = PWMNOMINAL;
  EnableInterrupts();
  LCDClear1();
  ControllerFlag = 0;
  Pause3();

    while(1){
        if(Bump_Read()){
            uint8_t bump = Bump_Read();
            Mode = 0;
            Motor_Stop();
            if(bump == 0x04 || bump == 0x08) { //center two bumps
                Motor_Backward(4000, 4000);
                Clock_Delay1ms(500);
                Motor_Left(1050, 4000);
                Clock_Delay1ms(1000);
            }
            if(bump == 0x02 || bump == 0x01) { //right bumps
                Motor_Backward(4000, 4000);
                Clock_Delay1ms(500);
                Motor_Left(1050, 2050);
                Clock_Delay1ms(1000);
            }
            if(bump == 0x20 || bump == 0x10) { //left bumps
                Motor_Backward(4000, 4000);
                Clock_Delay1ms(500);
                Motor_Right(2050, 1050);
                Clock_Delay1ms(1000);
            }
            Mode = 1;
        }
        if(ControllerFlag){
            LCDOut1();
            ControllerFlag = 0;
        }
  }
}

/**************Level 4******************************************/
scenario_t var;

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
void SysTick_Handler(void){
    if(Mode){
        // Determine set point
        if(Left > 172 && Right > 172) {
            SetPoint = (Left + Right) / 2;
        } else {
            SetPoint = 172;
        }

        //set error based off set point
        if(Left < Right) {
            Error2 = Left - SetPoint;
        }
        if(Right < Left) {
            Error2 = SetPoint - Right;
        }

        var = Classify(Left, Center, Right);
        if(var == Blocked) {
            Motor_Stop();
            Clock_Delay1ms(1000);
            Motor_Right(6000, 6000);
            Clock_Delay1ms(1000);
        }
        if(var == RightTooClose) {
            Motor_Stop();
            Clock_Delay1ms(1000);
            Motor_Left(1000, 4000);
            Clock_Delay1ms(1000);
        }
        if(var == CenterTooClose) {
            Motor_Stop();
            Clock_Delay1ms(1000);
            Motor_Right(4000, 1000);
            Clock_Delay1ms(1000);
        }
        if(var == LeftTurn) {
            Motor_Left(2000, 4000);
            Clock_Delay1ms(1000);
            if(var == RightTooClose || CenterTooClose || TeeJoint) {
                Motor_Stop();
                Clock_Delay1ms(1000);
                Motor_Backward(3000, 3000);
                Clock_Delay1ms(1000);
                Motor_Right(4000, 1000);
                Clock_Delay1ms(1000);
            }
        }
        if(var == RightTurn) {
            Motor_Right(3075, 1075);
            Clock_Delay1ms(1000);
        }
        if(var == TeeJoint) {
            Motor_Stop();
            Clock_Delay1ms(1000);
            Motor_Right(3075, 1050);
            Clock_Delay1ms(1000);
        }
        if(var == LeftJoint) {
            Motor_Forward(6000, 6000);
        }
        if(var == RightJoint) {
            Motor_Forward(6000, 6000);
        }
        if(var == CrossRoad) {
            Motor_Forward(6000, 6000);
        }

        // update duty cycle based on proportional control
        UR = UR + Kp * Error2;
        UL = UL - Kp * Error2;

        // check to ensure not too big of a swing
        if(UR < PWMIN) {
            UR = PWMIN;
        }
        if(UR > PWMAX) {
            UR = PWMAX;
        }
        if(UL < PWMIN) {
            UL = PWMIN;
        }
        if(UL > PWMAX) {
            UL = PWMAX;
        }

        // update motor values
        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }
}

// proportional control, wall distance
void Level_4(void){
    uint32_t raw17,raw12,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init(); //change this from lab 14 Bumpint_Init()
    Motor_Init();

    // user TimerA1 to sample the IR sensors at 2000 Hz
    TimerA1_Init(&IRsampling, 250);

    Motor_Stop();
    LCDClear2();
    Mode = 0;
    UR = UL = PWMNOMINAL;
    ADCflag = ControllerFlag = 0;   // semaphores

    ADC0_InitSWTriggerCh17_12_16();   // initialize channels 17,12,16
    ADC_In17_12_16(&raw17,&raw12,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw12,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

    // user SysTick to run the controller at 100 Hz with a priority of 2
    SysTick_Init(480000, 2);

    Pause3();

    EnableInterrupts();
    while(1){
        if(Bump_Read()){ // collision
            Mode = 0;
            Motor_Stop();
            Pause3();
        }
        if(ControllerFlag){ // 100 Hz, not real time
            LCDOut2();
            ControllerFlag = 0;
        }
    }
}

int main(void){
//    Program17_1();
//    Program17_2();
//    Program17_3(); //this is for level 1 now
//    Level_2();
//    Level_3();
    Level_4();
}
